#include "..\script_component.hpp"

params ["_unit"];

if (!isNull (_unit getVariable [QEGVAR(common,owner), objNull])) exitWith {};

[_unit, IS_UNCONSCIOUS(_unit)] call FUNC(setUnconsciousAnim);
[_unit, IS_UNCONSCIOUS(_unit)] call FUNC(waitForAnim);

_unit setVariable [QGVAR(deferredUnconsciousAnim), false];
