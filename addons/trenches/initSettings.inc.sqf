// Trenches dig/remove durations
[
    QGVAR(allowDiggingWithoutTool),
    "CHECKBOX",
    ["Allow Digging without Tool", "Allow players to dig trenches without needing a tool in their inventories"],
    LSTRING(Category),
    false,
    true
] call CBA_fnc_addSetting;

[
    QGVAR(smallEnvelopeDigDuration),
    "TIME",
    [LSTRING(SmallEnvelopeDigDuration_DisplayName), LSTRING(SmallEnvelopeDigDuration_Description)],
    LSTRING(Category),
    [5, 600, 20],
    true
] call CBA_fnc_addSetting;

[
    QGVAR(smallEnvelopeRemoveDuration),
    "TIME",
    [LSTRING(SmallEnvelopeRemoveDuration_DisplayName), LSTRING(SmallEnvelopeRemoveDuration_Description)],
    LSTRING(Category),
    [5, 600, 12],
    true
] call CBA_fnc_addSetting;

[
    QGVAR(bigEnvelopeDigDuration),
    "TIME",
    [LSTRING(BigEnvelopeDigDuration_DisplayName), LSTRING(BigEnvelopeDigDuration_Description)],
    LSTRING(Category),
    [5, 600, 25],
    true
] call CBA_fnc_addSetting;

[
    QGVAR(bigEnvelopeRemoveDuration),
    "TIME",
    [LSTRING(BigEnvelopeRemoveDuration_DisplayName), LSTRING(BigEnvelopeRemoveDuration_Description)],
    LSTRING(Category),
    [5, 600, 15],
    true
] call CBA_fnc_addSetting;
