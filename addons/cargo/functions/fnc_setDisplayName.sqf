#include "..\script_component.hpp"
/*
 * Author: Bloodshot
 * Set the display name of a cargo object or classname
 *
 * Arguments:
 * 0: Object <OBJECT>
 * 1: Display Name <STRING>
 *
 * Return Value:
 * None
 *
 * Example:
 * [object, "Resupply Crate"] call ace_cargo_fnc_setDisplayName
 *
 * Public: Yes
 */

params ["_object", "_displayName"];

_object setVariable [QGVAR(displayName), _displayName, true];
