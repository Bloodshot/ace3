[
    QEGVAR(medical,bleedingCoefficient),
    "SLIDER",
    [LSTRING(BleedingCoefficient_DisplayName), LSTRING(BleedingCoefficient_Description)],
    [ELSTRING(medical,Category), LSTRING(SubCategory)],
    [0, 25, 1, 1],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,painCoefficient),
    "SLIDER",
    [LSTRING(PainCoefficient_DisplayName), LSTRING(PainCoefficient_Description)],
    [ELSTRING(medical,Category), LSTRING(SubCategory)],
    [0, 25, 1, 1],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,ivFlowRate),
    "SLIDER",
    [LSTRING(IvFlowRate_DisplayName), LSTRING(IvFlowRate_Description)],
    [ELSTRING(medical,Category), LSTRING(SubCategory)],
    [0, 25, 1, 1],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,dropWeaponUnconsciousChance),
    "SLIDER",
    [LSTRING(DropWeaponUnconsciousChance_DisplayName), LSTRING(DropWeaponUnconsciousChance_Description)],
    ELSTRING(medical,Category),
    [0, 1, 0, 2, true],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,unconsciousnessThreshold),
    "SLIDER",
    ["Unconsciousness Threshold", "Point at which a unit will fall unconscious. Lower numbers will make it easier to fall unconscious."],
    [ELSTRING(medical,Category), "Unconsciousness"],
    [0, 25, 3, 1],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,wakeupThreshold),
    "SLIDER",
    ["Wakeup Threshold", "Point at which a unit will wake up from unconsciousness."],
    [ELSTRING(medical,Category), "Unconsciousness"],
    [0, 25, 2.75, 1],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,painUnconsciousnessFactor),
    "SLIDER",
    ["Pain Unconsciousness Factor", "How heavily to weight pain when determining if a unit should be conscious"],
    [ELSTRING(medical,Category), "Unconsciousness"],
    [0, 25, 2.5, 1],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,hrUnconsciousnessFactor),
    "SLIDER",
    ["Heartrate Unconsciousness Factor", "How heavily to weight heartrate when determining if a unit should be conscious"],
    [ELSTRING(medical,Category), "Unconsciousness"],
    [0, 25, 1, 1],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,bpUnconsciousnessFactor),
    "SLIDER",
    ["Blood Pressure Unconsciousness Factor", "How heavily to weight blood pressure when determining if a unit should be conscious"],
    [ELSTRING(medical,Category), "Unconsciousness"],
    [0, 25, 1, 1],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,staminaUnconsciousnessFactor),
    "SLIDER",
    ["Stamina Unconsciousness Factor", "How heavily to weight stamina when determining if a unit should be conscious"],
    [ELSTRING(medical,Category), "Unconsciousness"],
    [0, 25, 1, 1],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,bloodLossUnconsciousnessFactor),
    "SLIDER",
    ["Blood Loss Unconsciousness Factor", "How heavily to weight active bleeding when determining if a unit should be conscious"],
    [ELSTRING(medical,Category), "Unconsciousness"],
    [0, 25, 2, 1],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,shockUnconsciousnessFactor),
    "SLIDER",
    ["Shock Unconsciousness Factor", "How heavily to weight shock due to wounds when determining if a unit should be conscious"],
    [ELSTRING(medical,Category), "Unconsciousness"],
    [0, 25, 1, 1],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,woundUnconsciousnessFactor),
    "SLIDER",
    ["Wound Unconsciousness Factor", "How heavily to weight accumulated wounds when determining if a unit should be conscious"],
    [ELSTRING(medical,Category), "Unconsciousness"],
    [0, 25, 0.5, 1],
    true
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,epiUnconsciousnessFactor),
    "SLIDER",
    ["Epinephrine Unconsciousness Factor", "How heavily to weight epinephrine when determining if a unit should be conscious"],
    [ELSTRING(medical,Category), "Unconsciousness"],
    [0, 25, 3, 1]
] call CBA_fnc_addSetting;

[
    QEGVAR(medical,secondWindUnconsciousnessFactor),
    "SLIDER",
    ["Second Wind Unconsciousness Factor", "How heavily to weight how long a unit's been unconsciouss when determining if they should regain it"],
    [ELSTRING(medical,Category), "Unconsciousness"],
    [0, 25, 1, 1]
] call CBA_fnc_addSetting;
