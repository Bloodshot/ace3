#include "..\script_component.hpp"
/*
 * Author: Bloodshot
 * Returns the unit's knockout factor; i.e., how close the unit is to falling
 * unconscious. Used by handleUnitVitals to determine when a unit should fall
 * unconscious, or regain consciousness.
 *
 * Arguments:
 * 0: The Unit <OBJECT>
 *
 * Return Value:
 * Knockout factor <NUMBER>
 *
 * Example:
 * [player] call ace_medical_status_fnc_getKnockOutFactor
 *
 * Public: No
 */

params ["_unit"];

// Pain
private _pain = GET_PAIN_PERCEIVED(_unit);
private _painFactor = (_pain ^ 2) * EGVAR(medical,painUnconsciousnessFactor);

// Heartrate
private _heartrate = GET_HEART_RATE(_unit);
private _heartrateFactor = 0;
if (_heartrate < 50) then {
    _heartrateFactor = (linearconversion [30, 50, _heartrate, 1, 0, true]) ^ 2;
};

if (_heartrate > 140) then {
   _heartrateFactor = (linearconversion [120, 200, _heartrate, 0, 0.9, true]) ^ 2;
};

_heartrateFactor = _heartrateFactor * EGVAR(medical,hrUnconsciousnessFactor);

// Blood Pressure (Diastolic)
private _bloodpressure = GET_BLOOD_PRESSURE(_unit) select 0;
private _bloodpressureFactor = 0;

if (_bloodpressure < 70) then {
    _bloodpressureFactor = (linearconversion [30, 70, _bloodpressure, 1, 0, false]) ^ 2;
};

if (_bloodpressure > 140) then {
    _bloodpressureFactor = (linearconversion [140, 200, _bloodpressure, 0, 0.55, true]) ^ 2;
};

_bloodpressureFactor = _bloodpressureFactor * EGVAR(medical,bpUnconsciousnessFactor);

// Stamina (hacky)
private _stamina = _unit getVariable [QEGVAR(advanced_fatigue,aimFatigue), getFatigue _unit];
private _staminaFactor = _stamina * EGVAR(medical,staminaUnconsciousnessFactor);

// Blood loss
private _bloodLoss = [_unit] call FUNC(getBloodLoss);
private _bloodLossFactor = (linearconversion [0, 0.7, _bloodLoss, 0, 1, false]);
_bloodLossFactor = _bloodLossFactor * EGVAR(medical,bloodLossUnconsciousnessFactor);

// Epinephrine
private _epinephrine = [_unit, "Epinephrine", false] call FUNC(getMedicationCount);
private _epinephrineFactor = _epinephrine * EGVAR(medical,epiUnconsciousnessFactor);

// Time Factor
private _randomUnconsciousnessPhase = _unit getVariable [QGVAR(randomUnconsciousnessPhase), 0];

// Period is two minutes (sin in degrees => 360 / 3 = 120s)
private _timeFactor = (sin ((CBA_missionTime + _randomUnconsciousnessPhase) * 3)) * 0.5;

// Shock Factor, due to wounds
private _shock = _unit getVariable [QGVAR(shock), 0];
private _shockFactor = _shock * EGVAR(medical,shockUnconsciousnessFactor);

// Wound factor, cumulative
private _bodyPartDamage = 0;
{
    _bodyPartDamage = _bodyPartDamage + _x;
} forEach (_unit getVariable [QEGVAR(medical,bodyPartDamage), []]);

private _woundUnconsciousnessFactor = linearconversion [0, 36, _bodyPartDamage,
                                                        0, EGVAR(medical,woundUnconsciousnessFactor), true];

private _secondWindFactor = (_unit getVariable [QGVAR(secondWind), 0]) * EGVAR(medical,secondWindUnconsciousnessFactor);

_painFactor + _heartrateFactor + _bloodpressureFactor + _staminaFactor + _bloodLossFactor
    - _epinephrineFactor + _timeFactor + _shockFactor + _woundUnconsciousnessFactor
    - _secondWindFactor
