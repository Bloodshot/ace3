[
    QGVAR(fatalInjuriesPlayer),
    "LIST",
    [LSTRING(FatalInjuriesPlayer_DisplayName), LSTRING(FatalInjuriesPlayer_Description)],
    [ELSTRING(medical,Category), LSTRING(SubCategory)],
    [
        [FATAL_INJURIES_ALWAYS, FATAL_INJURIES_NEVER],
        [ELSTRING(common,Always), ELSTRING(common,Never)],
        0
    ],
    true
] call CBA_fnc_addSetting;

[
    QGVAR(fatalInjuriesAI),
    "LIST",
    [LSTRING(FatalInjuriesAI_DisplayName), LSTRING(FatalInjuriesAI_Description)],
    [ELSTRING(medical,Category), LSTRING(SubCategory)],
    [
        [FATAL_INJURIES_ALWAYS, FATAL_INJURIES_NEVER],
        [ELSTRING(common,Always), ELSTRING(common,Never)],
        0
    ],
    true
] call CBA_fnc_addSetting;

[
    QGVAR(AIUnconsciousness),
    "CHECKBOX",
    [LSTRING(AIUnconsciousness_DisplayName), LSTRING(AIUnconsciousness_Description)],
    [ELSTRING(medical,Category), LSTRING(SubCategory)],
    true,
    true
] call CBA_fnc_addSetting;

[
    QGVAR(fatalVitalsTime),
    "TIME",
    ["Fatal Vitals Time", "Controls how long it takes to die from fatal vitals"],
    [ELSTRING(medical,Category), LSTRING(SubCategory)],
    [1, 3600, 60],
    true
] call CBA_fnc_addSetting;
