// Manual transitions applied to this statemachine
//  - fnc_resetStateDefault on unit respawn
class ACE_Medical_StateMachine {
    list = QUOTE(call EFUNC(common,getLocalUnits));
    skipNull = 1;
    class Default {
        onState = QFUNC(handleStateDefault);
        class CriticalInjuryOrVitals {
            targetState = "Unconscious";
            events[] = {QEGVAR(medical,CriticalInjury), QEGVAR(medical,CriticalVitals)};
            condition = QUOTE(!([_this] call FUNC(isUnconsciousnessAllowed)));
        };
        class Injury {
            targetState = "Injured";
            events[] = {QEGVAR(medical,injured), QEGVAR(medical,LoweredVitals),
                        QEGVAR(medical,CriticalInjury), QEGVAR(medical,CriticalVitals)};
        };
        class KnockOut {
            targetState = "Unconscious";
            events[] = {QEGVAR(medical,knockOut)};
        };
        class FatalVitals {
            targetState = "Unconscious";
            events[] = {QEGVAR(medical,FatalVitals), QEGVAR(medical,Bleedout)};
        };
        class FatalInjury {
            targetState = "FatalInjury";
            events[] = {QEGVAR(medical,FatalInjury)};
        };
    };
    class Injured {
        onState = QFUNC(handleStateInjured);
        class FullHeal {
            targetState = "Default";
            events[] = {QEGVAR(medical,FullHeal)};
        };
        class CriticalInjuryOrVitals {
            targetState = "Unconscious";
            events[] = {QEGVAR(medical,CriticalInjury), QEGVAR(medical,CriticalVitals)};
            condition = QUOTE(!([_this] call FUNC(isUnconsciousnessAllowed)));
        };
        class KnockOut {
            targetState = "Unconscious";
            events[] = {QEGVAR(medical,knockOut)};
        };
        class FatalVitals {
            targetState = "Unconscious";
            events[] = {QEGVAR(medical,FatalVitals), QEGVAR(medical,Bleedout)};
        };
        class FatalInjury {
            targetState = "FatalInjury";
            events[] = {QEGVAR(medical,FatalInjury)};
        };
    };
    class Unconscious {
        onState = QFUNC(handleStateUnconscious);
        onStateEntered = QFUNC(enteredStateUnconscious);
        class DeathAI {
            targetState = "Dead";
            condition = QUOTE(!([_this] call FUNC(isUnconsciousnessAllowed)));
        };
        class WakeUp {
            targetState = "Injured";
            events[] = {QEGVAR(medical,WakeUp)};
            onTransition = QUOTE([ARR_2(_this,false)] call EFUNC(medical_status,setUnconsciousState));
        };
        class Bleedout {
            targetState = "Dead";
            events[] = {QEGVAR(medical,Bleedout)};
        };
        class FatalVitals {
            targetState = "FatalVitals";
            events[] = {QEGVAR(medical,FatalVitals)};
        };
        class FatalInjury {
            targetState = "FatalInjury";
            events[] = {QEGVAR(medical,FatalInjury)};
        };
    };
    class FatalVitals {
        onState = QFUNC(handleStateFatalVitals);
        onStateEntered = QFUNC(enteredStateFatalVitals);
        onStateLeaving = QFUNC(leftStateFatalVitals);
        class DeathAI {
            // If an AI unit reanimates, they will immediately die upon entering unconsciousness if AI Unconsciousness is disabled
            // As a result, we immediately kill the AI unit since fatal vitals is effectively useless for it
            targetState = "Dead";
            condition = QUOTE(!([_this] call FUNC(isUnconsciousnessAllowed)));
        };
        class Stabilization {
            targetState = "Unconscious";
            condition = QFUNC(conditionStableVitals);
        };
        class Timeout {
            targetState = "Dead";
            condition = QFUNC(conditionFatalVitalsTimer);
        };
        class Bleedout {
            targetState = "Dead";
            events[] = {QEGVAR(medical,Bleedout)};
        };
        class FatalInjury {
            targetState = "FatalInjury";
            events[] = {QEGVAR(medical,FatalInjury)};
        };
    };
    class FatalInjury {
        // Transition state for handling instant death from fatal injuries
        // This state raises the next transition in the same frame
        onStateEntered = QFUNC(enteredStateFatalInjury);
        class SecondChance {
            events[] = {QEGVAR(medical,FatalInjuryInstantTransition)};
            targetState = "Unconscious";
            condition = QFUNC(conditionSecondChance);
            onTransition = QFUNC(transitionSecondChance);
        };
        class Death {
            events[] = {QEGVAR(medical,FatalInjuryInstantTransition)};
            targetState = "Dead";
        };
    };
    class Dead {
        // When the unit is killed it's no longer handled by the statemachine
        onStateEntered = QFUNC(enteredStateDeath);
    };
};
