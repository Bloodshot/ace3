#include "..\script_component.hpp"
/*
 * Author: BaerMitUmlaut
 * Checks if the fatal vitals timer ran out.
 *
 * Arguments:
 * 0: The Unit <OBJECT>
 *
 * Return Value:
 * None
 *
 * Example:
 * [player] call ace_medical_statemachine_fnc_conditionFatalVitalsTimer
 *
 * Public: No
 */

params ["_unit"];

(_unit getVariable [QGVAR(fatalVitalsTimeLeft), -1]) <= 0
