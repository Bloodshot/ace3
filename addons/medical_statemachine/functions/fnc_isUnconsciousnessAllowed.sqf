#include "..\script_component.hpp"
/*
 * Author: Bloodshot
 * Checks if a unit is able to be unconscious
 *
 * Arguments:
 * 0: The Unit <OBJECT>
 *
 * Return Value:
 * None
 *
 * Example:
 * [player] call ace_medical_statemachine_fnc_unconsciousnessAllowed
 *
 * Public: No
 */

params ["_unit"];

private _unitVar = _unit getVariable QEGVAR(medical,allowUnconsciousness);
if (!isNil "_unitVar" && {typeName _unitVar == "BOOL"}) exitWith {_unitVar};

if (isPlayer _unit) exitWith {true};

GVAR(AIUnconsciousness)
