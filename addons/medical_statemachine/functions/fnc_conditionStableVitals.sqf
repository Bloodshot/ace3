#include "..\script_component.hpp"
/*
 * Author: Bloodshot
 * Determine whether the unit's vitals are *non-lethal*, in order
 * to halt the lethal vitals countdown. This is less strict than
 * ace_medical_status_fnc_hasStableVitals.sqf
 *
 * Arguments:
 * 0: Unit <OBJECT>
 *
 * Return Value:
 * None
 *
 * Example:
 * [bob] call ace_medical_statemachine_fnc_conditionStableVitals.sqf
 *
 * Public: No
 */

params ["_unit"];

if (GET_BLOOD_VOLUME(_unit) < BLOOD_VOLUME_CLASS_4_HEMORRHAGE) exitWith { false };

private _cardiacOutput = [_unit] call EFUNC(medical_status,getCardiacOutput);

private _bloodPressure = GET_BLOOD_PRESSURE(_unit);
_bloodPressure params ["_bloodPressureL", "_bloodPressureH"];
private _heartRate = GET_HEART_RATE(_unit);
if (_bloodPressureL < 40 && {_bloodPressureH < 50} && {_heartRate < 40}) exitWith { false };

if (_heartRate < 30 || {_heartRate > 210}) exitWith { false };

if (_bloodPressureL > 190) exitWith { false };

true
