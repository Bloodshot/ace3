#include "..\script_component.hpp"
/*
 * Author: RedBery
 * Handles a unit leaving fatal vitals (calls for a status update).
 * Clears countdown timer variables.
 *
 * Arguments:
 * 0: The Unit <OBJECT>
 *
 * Return Value:
 * None
 *
 * Example:
 * [player] call ace_medical_statemachine_fnc_leftStateFatalVitals
 *
 * Public: No
 */

params ["_unit"];
TRACE_1("leftStateFatalVitals",_unit);

_unit setVariable [QGVAR(fatalVitalsTimeLeft), nil];
_unit setVariable [QGVAR(fatalVitalsTimeLastUpdate), nil];
