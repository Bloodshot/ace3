#include "..\script_component.hpp"
/*
 * Author: Bloodshot
 * Handles the fatal vitals state
 *
 * Arguments:
 * 0: The Unit <OBJECT>
 *
 * Return Value:
 * None
 *
 * Example:
 * [player] call ace_medical_statemachine_fnc_handleStateFatalVitals
 *
 * Public: No
 */

params ["_unit"];

// If the unit died the loop is finished
if (!alive _unit) exitWith {};
if (!local _unit) exitWith {};

[_unit] call EFUNC(medical_vitals,handleUnitVitals);

private _timeDiff = CBA_missionTime - (_unit getVariable [QGVAR(fatalVitalsTimeLastUpdate), 0]);
if (_timeDiff >= 1) then {
    _timeDiff = _timeDiff min 10;
    _unit setVariable [QGVAR(fatalVitalsTimeLastUpdate), CBA_missionTime];
    private _timeLeft = _unit getVariable [QGVAR(fatalVitalsTimeLeft), -1];
    TRACE_2("fatal vitals life tick",_unit,_timeDiff);
    _timeLeft = _timeLeft - _timeDiff; // negative values are fine
    _unit setVariable [QGVAR(fatalVitalsTimeLeft), _timeLeft];
};

