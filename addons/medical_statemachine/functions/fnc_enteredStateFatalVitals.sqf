#include "..\script_component.hpp"
/*
 * Author: BaerMitUmlaut
 * Handles a unit entering fatal vitals (calls for a status update).
 * Sets required variables for countdown timer until death.
 *
 * Arguments:
 * 0: The Unit <OBJECT>
 *
 * Return Value:
 * None
 *
 * Example:
 * [player] call ace_medical_statemachine_fnc_enteredStateFatalVitals
 *
 * Public: No
 */

params ["_unit"];

// 10% possible variance in fatal vitals time
private _time = GVAR(fatalVitalsTime);
_time = _time + _time * random [-0.1, 0, 0.1];

_unit setVariable [QGVAR(fatalVitalsTimeLeft), _time];
_unit setVariable [QGVAR(fatalVitalsTimeLastUpdate), CBA_missionTime];

TRACE_3("enteredStateFatalVitals",_unit,_time,CBA_missionTime);
