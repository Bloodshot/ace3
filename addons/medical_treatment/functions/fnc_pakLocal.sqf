#include "..\script_component.hpp"
/*
 * Author: Bloodshot
 * Local callback for apply a Personal Aid Kit to a patient.
 *
 * Arguments:
 * 0: Patient <OBJECT>
 *
 * Return Value:
 * None
 *
 * Example:
 * [player] call ace_medical_treatment_fnc_pakLocal
 *
 * Public: No
 */

params ["_patient"];
TRACE_1("pakLocal",_patient);

if (!alive _patient) exitWith {};

private _bloodVolume = _patient getVariable [VAR_BLOOD_VOL, DEFAULT_BLOOD_VOLUME];
_bloodVolume = (_bloodVolume + GVAR(bloodRestoredPAK)) min DEFAULT_BLOOD_VOLUME;
private _ivBags = _patient getVariable [QEGVAR(medical,ivBags), nil];

[_patient] call FUNC(fullHealLocal);

_patient setVariable [VAR_BLOOD_VOL, _bloodVolume, true];
_patient setVariable [QEGVAR(medical,ivBags), _ivBags, true];
