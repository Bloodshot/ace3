#include "..\script_component.hpp"
/*
 * Author: Bloodshot
 * Apply a Personal Aid Kit to a patient
 *
 * Arguments:
 * 0: Medic <OBJECT>
 * 1: Patient <OBJECT>
 * 2: Body Part <STRING>
 * 3: Treatment <STRING>
 *
 * Return Value:
 * None
 *
 * Example:
 * [player, cursorObject] call ace_medical_treatment_fnc_pak
 *
 * Public: No
 */

params ["_medic", "_patient"];

[_patient, "activity", LSTRING(Activity_fullHeal), [[_medic, false, true] call EFUNC(common,getName)]] call FUNC(addToLog);

[QGVAR(pakLocal), [_patient], _patient] call CBA_fnc_targetEvent;
