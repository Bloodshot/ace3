// This config accepts both item type numbers and item class names
// Item type numbers need the prefix ItemType_, so for example ItemType_401
// Class names need no special prefix
class EGVAR(medical,replacementItems) {
    DOUBLES(ItemType,TYPE_FIRST_AID_KIT)[] = {
        {"ACE_fieldDressing", 1},
        {"ACE_packingBandage", 2},
        {"ACE_morphine", 1},
        {"ACE_tourniquet", 1}
    };
    DOUBLES(ItemType,TYPE_MEDIKIT)[] = {
        {"ACE_fieldDressing", 2},
        {"ACE_packingBandage", 5},
        {"ACE_epinephrine", 2},
        {"ACE_morphine", 3},
        {"ACE_salineIV_500", 3},
        {"ACE_tourniquet", 2},
        {"ACE_splint", 2},
        {"ACE_adenosine", 1},
        {"ACE_personalAidKit", 2}
    };
    ACE_atropine[] = {
        {"ACE_adenosine", 1}
    };
};
