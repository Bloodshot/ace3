#include "..\script_component.hpp"
/*
 * Author: Bloodshot
 * Displays and blinks a text notification when a player is currently receiving
 * treatment
 *
 * Arguments:
 * 0: Whether to enable the text
 *
 * Return Value:
 * None
 *
 * Example:
 * [] call ace_medical_feedback_fnc_effectReceivingTreatment
 *
 * Public: No
 */
params ["_enable"];

if (_enable) then {
    if (isNil QGVAR(receivingTreatmentPFH)) then {
        "ACE_ReceivingTreatment" cutRsc ["ACE_ReceivingTreatment", "PLAIN", 0, true];
        _display = uiNamespace getVariable ["ACE_ReceivingTreatment", displayNull];
        if (isNull _display) exitwith {};

        _multiplier = GVAR(receivingTreatmentSize);
        _width = 0.1 * safeZoneW * _multiplier;
        _height = 0.025 * safeZoneH * _multiplier;
        _x = 0.5 * safeZoneW - 0.5 * _width + safeZoneX;
        _y = 0.9 * safeZoneH - 0.5 * _height + safeZoneY;
        _size = 0.0275 * safeZoneH * _multiplier;

        _text = _display displayCtrl 1000;
        _text ctrlSetPosition [_x, _y, _width, _height];
        _text ctrlSetFontHeight _size;
        _text ctrlCommit 0;

        _startTime = time;

        GVAR(receivingTreatmentPFH) = [{
            params ["_display", "_startTime"];
            if (isNull _display) exitwith {};

            // Blinking effect
            _text = _display displayCtrl 1000;
            _alpha = (2 * (sin ((time - _startTime - 0.5) * 180) + 1)) min 1;
            _text ctrlSetTextColor [1, 1, 1, _alpha];
        }, 0, _display, _startTime] call CBA_fnc_addPerFrameHandler;
    };
} else {
    if (!isNil QGVAR(receivingTreatmentPFH)) then {
        "ACE_ReceivingTreatment" cutFadeOut 0.5;
        [GVAR(receivingTreatmentPFH)] call CBA_removePerFrameHandler;
        GVAR(receivingTreatmentPFH) = nil;
    };
};
