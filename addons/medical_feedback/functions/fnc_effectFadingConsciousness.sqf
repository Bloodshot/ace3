#include "..\script_component.hpp"

params ["_enable", "_intensity"];

if (!_enable) exitWith {
    GVAR(ppFading) ppEffectEnable false;
};
GVAR(ppFading) ppEffectEnable true;

// Increment tick counter
private _fadingTick = missionNamespace getVariable [QGVAR(fadingTick), 0];
_fadingTick = (_fadingTick + 1) mod 30;

switch (true) do {
    // Quickly fade out
    case (_intensity == 0): {
        GVAR(ppFading) ppEffectAdjust [1,1,0,[0,0,0,0.95],[0,0,0,1],[1,1,1,1],[8,4,0,0,0,0.5,2]];
        GVAR(ppFading) ppEffectCommit 2;
    };

    // Tracking intensity
    case (_fadingTick < 25): {
        private _radius = linearConversion [0, 1, _intensity, 0.9, 0];
        GVAR(ppFading) ppEffectAdjust [1,1,0,[0,0,0,0.95],[0,0,0,1],[1,1,1,1],[2 * _radius,_radius,0,0,0,0.5,2]];
        // If _intensity is constant, this should be linear (fade does not accelerate)
        GVAR(ppFading) ppEffectCommit (25 - _fadingTick);
    };

    // Fade out
    case (_fadingTick == 25): {
        GVAR(ppFading) ppEffectAdjust [1,1,0,[0,0,0,0.95],[0,0,0,1],[1,1,1,1],[2,1,0,0,0,0.5,2]];
        GVAR(ppFading) ppEffectCommit 5;
    };

    // 5 second cool off
};

missionNamespace setVariable [QGVAR(fadingTick),_fadingTick];
