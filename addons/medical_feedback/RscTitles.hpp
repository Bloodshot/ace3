
class RscText;
class RscTitles {
#ifdef DISABLE_VANILLA_BLOOD_TEXTURES
    // Disables blood texture overlay
    class RscHealthTextures {
        class controls {
            class Flame_1;
            class Blood_1: Flame_1 {
                text = ""; //"A3\Ui_f\data\igui\rsctitles\HealthTextures\blood_lower_ca.paa";
            };
            class Blood_2: Flame_1 {
                text = ""; //"A3\Ui_f\data\igui\rsctitles\HealthTextures\blood_middle_ca.paa";
            };
            class Blood_3: Flame_1 {
                text = ""; //"A3\Ui_f\data\igui\rsctitles\HealthTextures\blood_upper_ca.paa";
            };
        };
    };
#endif

    class ACE_ReceivingTreatment {
        idd = 9951;
        enableSimulation = 1;
        movingEnable = 0;
        fadeIn = 0;
        fadeOut = 1;
        duration = 10e10;
        onLoad = "uiNamespace setVariable ['ACE_ReceivingTreatment', _this select 0];";
        class controls {
            class Text : RscText {
                idc = 1000;
                text = "You are being treated.";
                style = 0x02;
                colorText[] = {1,1,1,0};
            };
        };
    };
};
